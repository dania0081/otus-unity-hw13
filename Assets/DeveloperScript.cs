using UnityEditor;

public class DeveloperScript
{
    [MenuItem("Developer/Stop Game %e")]
    private static void StopGame()
    {
        if (!EditorApplication.isPlaying)
        {
            return;
        }

        EditorApplication.isPaused = true;
    }
}